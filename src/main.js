// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueSweetalert2 from 'vue-sweetalert2';


import { library } from '@fortawesome/fontawesome-svg-core'
import { faBars, faArrowLeft, faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import socketio from 'socket.io-client';
import VueSocketio from 'vue-socket.io';

import { get_cookie } from "./assets/js/functions";

import VuePersianDatetimePicker from 'vue-persian-datetime-picker';
Vue.component('date-picker', VuePersianDatetimePicker);

Vue.use(require('vue-moment-jalaali'));

import 'bootstrap';

let user_id = get_cookie('user_id');
if(user_id){
    const socketInstance = socketio('http://portal.uberirani.com:3229/passengers', {
        transports: ['websocket'],
        query: {
            id : user_id,
            token: "4qvj2qlb9pgh6o8akafJo71vdwy144h65q"
        }
    });
    Vue.use(VueSocketio, socketInstance);
}
Vue.use(VueSweetalert2);

library.add(faBars);
library.add(faArrowLeft);
library.add(faStar);
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBv23iMcm3AvqA0VmWRhlvKtDAZoyc-x4Q',
        region: 'IR',

        language:'fa',
        libraries: 'places', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)

        //// If you want to set the version, you can do so:
        // v: '3.26',
    },

    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    // autobindAllEvents: false,

    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then disable the following:
    // installComponents: true,
});


/* eslint-disable no-new */
$.vue = new Vue({
    el: '#app',
    components: {App},
    template: '<App/>',
});
