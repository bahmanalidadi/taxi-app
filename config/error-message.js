export const error_messages = {
    100: 'id cannot be empty',
    101: 'این حساب کاربری منقضی شده است',
    102: 'این حساب کاربری غیر فعال است',
    103: 'این حساب کاربری هنوز بررسی و تایید نشده است',
    104: 'این حساب کاربری حذف گردیده',
    105: 'این حساب کاربری مسدود است',
    106: 'کلید وب سرویس نا معتبر است',
    107: 'اطلاعات ورود نا معتبر است',
    108: 'this method is not working via get',
    109: 'این حساب کاربری فعال نشده',
    110: 'راننده درخواستی پیدا نشد',
    111: 'مسافر درخواستی پیدا نشد',
    112: 'پیامک ارسال نشد',
    113: 'کد فعال سازی منقضی شده است',
    114: 'کد فعال سازی اشتباه است',
    115: 'هیچ سرویسی پیدا نشد',
    116: 'mentioned trip does not exist',
    117: 'error in saving data into DB',
    118: 'the requested driver does not exist',
    119: 'There is no payment for this trip',
    120: 'هیچ چیزی برگردانده نشد',
    121: 'trip offer has not been accepted yet',
    122: 'driver has not arrived yet',
    123: 'حساب کاربری  با این مشخصات وجود دارد',
    124: 'کد تخفیف اشتباه است',
    131: 'Can’t do that right now',
    601: 'Invalid payment amount',
    602: 'Invalid token',
    603: 'Insufficient credits',
    604: 'Sufficient credit !',
    605: 'Already paid',
};
